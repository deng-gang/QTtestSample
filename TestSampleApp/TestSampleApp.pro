#-------------------------------------------------
#
# Project created by QtCreator 2020-09-07T12:34:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestSampleApp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

#include($$PWD/../UI.pri)

# here's sample of how to define your variable
# $$YOUR_VARIABLE and $${YOUR_VARIABLE} means extract the variable
# $$(YOUR_VARIABLE) means an env varaiable
# YOUR_MESSAGE = "OffsetPressApp.pro"
# message("Now is building:")
# message($$YOUR_MESSAGE)

# I already define TOPDIR in OffsetPress.pro, but can not use it here
# So I have to fine it again here.
TOPDIR = $$PWD/..

# IF I involve files by pri file, I can not see them in QtCreator
# So have to add these file urgly here and in test project
UIPATH = $$TOPDIR/TestSampleApp/UI
SOURCES += \
        $${UIPATH}/MainWindow/mainwindow.cpp

HEADERS += \
    $$UIPATH/MainWindow/mainwindow.h

FORMS += \
        $${UIPATH}/MainWindow/mainwindow.ui

INCLUDEPATH += $${UIPATH}/MainWindow


SOURCES += \
        main.cpp
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=
