#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    friend class MainWindowTester;    // here export private member to test class
    // https://zhuanlan.zhihu.com/p/40901748
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
