QT       += gui testlib
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS


# I already define TOPDIR in OffsetPress.pro, but can not use it here
# So I have to fine it again here.
TOPDIR = $$PWD/..

# IF I involve files by pri file, I can not see them in QtCreator
# So have to add these file urgly here and in test project
UIPATH = $$TOPDIR/TestSampleApp/UI
SOURCES += \
        $${UIPATH}/MainWindow/mainwindow.cpp \
        commoditydialog.cpp \
        commoditydialogtest.cpp \
        commoditywidget.cpp \
        commodity.cpp \
        commoditytest.cpp \
        testqstring.cpp

HEADERS += \
    $$UIPATH/MainWindow/mainwindow.h \
    commoditydialog.h \
    commoditydialogtest.h \
    commoditywidget.h \
    AutoTest.h \
    commodity.h \
    commoditytest.h \
    testqstring.h

FORMS += \
        $${UIPATH}/MainWindow/mainwindow.ui \
        commoditydialog.ui \
        commoditywidget.ui

INCLUDEPATH += $${UIPATH}/MainWindow



# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
