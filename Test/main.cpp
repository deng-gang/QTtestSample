//#include "commoditytest.h"
#include "AutoTest.h"

#if 0 // here's code to show a widget . dont't run auto test
#include "commoditydialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CommodityDialog w;
    w.show();

    return a.exec();
}
#else
TEST_MAIN

// to test more than 1 class, we use marco in AutoTest.h
//QTEST_MAIN(CommodityTest)

#endif
