#ifndef COMMODITYDIALOGTEST_H
#define COMMODITYDIALOGTEST_H
#include <QtTest/QtTest>
#include "AutoTest.h"

class CommodityDialogTest : public QObject
{
    Q_OBJECT
public:
    CommodityDialogTest();

private slots:
    void case_dialog_popup_msgbox();
};

#endif // COMMODITYDIALOGTEST_H
