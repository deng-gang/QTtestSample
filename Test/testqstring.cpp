#include <QtTest/QtTest>
#include "testqstring.h"

void TestQString::toUpper()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("HELLO"));
}


DECLARE_TEST(TestQString)
