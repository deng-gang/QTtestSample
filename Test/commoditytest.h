#ifndef COMMODITYTEST_H
#define COMMODITYTEST_H

#include <QtTest/QtTest>
#include "AutoTest.h"
class CommodityTest : public QObject
{
    Q_OBJECT
private slots:
    void case1_serialNum();
    void case2_name();
    void case3_consting();
    void case4_price();
    void case5_profit();

private slots:
    //成本
    void case1_gui_costing();
    //价格
    void case2_gui_price();
    //利润
    void case3_gui_profit();
};


//DECLARE_TEST(CommodityTest)
#endif // COMMODITYTEST_H
