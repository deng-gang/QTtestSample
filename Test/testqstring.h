#ifndef TESTQSTRING_H
#define TESTQSTRING_H

#include <QtTest/QtTest>
#include "AutoTest.h"

class TestQString: public QObject
{
    Q_OBJECT
private slots:
    void toUpper();
};
//DECLARE_TEST(TestQString)
#endif // TESTQSTRING_H
