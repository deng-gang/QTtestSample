#ifndef COMMODITYDIALOG_H
#define COMMODITYDIALOG_H

#include <QDialog>

namespace Ui {
class CommodityDialog;
}

class CommodityDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CommodityDialog(QWidget *parent = nullptr);
    ~CommodityDialog();
    friend class CommodityDialogTest; // CommodityDialogTest need access private member of CommodityDialog
private slots:
    void on_pushButton_PopUPOK_clicked();

private:
    Ui::CommodityDialog *ui;
};

#endif // COMMODITYDIALOG_H
