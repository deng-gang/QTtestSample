#include "commoditydialog.h"
#include "ui_commoditydialog.h"
#include "QMessageBox"

CommodityDialog::CommodityDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CommodityDialog)
{
    ui->setupUi(this);
}

CommodityDialog::~CommodityDialog()
{
    delete ui;
}

void CommodityDialog::on_pushButton_PopUPOK_clicked()
{
    QMessageBox::information(this, "this is my title", "this is my info",
                               QMessageBox::Ok,QMessageBox::NoButton);
}
